import './App.css'
import { MicrophoneIcon } from '@heroicons/react/24/solid'
import axios from 'axios'

function App() {
    const handleTalkback = () => {
        console.log('make api call')
        axios.get(`http://localhost:5000/api/talkback`)
        .then((result) => {
            console.log(result)
        })
        .catch((error) => {
            console.log(error)
        })
    }

    return (
        <div className="App">
            <div className="headingBar">
                <h3>Virtual Assistant</h3>
            </div>
            <div className="app">
                <MicrophoneIcon
                    onClick={handleTalkback}
                    className="microphoneIcon"
                />

                <input
                    type="text"
                    placeholder="Command"
                    className="inputField"
                />
            </div>
        </div>
    )
}

export default App